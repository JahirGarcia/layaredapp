﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Contracts;
using DataAccess.Entities;
using DataAccess.Repositories;
using Domain.ValueObjects;

namespace Domain.Models
{
    public class EmployeeModel
    {
        private int id;
        private string code;
        private string name;
        private string email;
        private DateTime birthday;
        private int age;

        private IEmployeeRepository employeeRepository;
        private EntityState state;

        public int Id { get => id; set => id = value; }

        [Required(ErrorMessage = "The field employee code is required")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Employee code must be only digits")]
        [StringLength(maximumLength:10, MinimumLength = 10, ErrorMessage = "Employee code must be 10 digits")]
        public string Code { get => code; set => code = value; }

        [Required]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "The field name must be only letters")]
        [StringLength(maximumLength:100, MinimumLength = 3)]
        public string Name { get => name; set => name = value; }

        [Required]
        [EmailAddress]
        public string Email { get => email; set => email = value; }
        public DateTime Birthday { get => birthday; set => birthday = value; }
        public int Age { private get => age; set => age = value; }
        public EntityState State { private get => state; set => state = value; }

        public EmployeeModel()
        {
            employeeRepository = new EmployeeRepository();
        }

        public string SaveChanges()
        {
            string message = "";
            try
            {
                var employeeDataModel = new Employee();
                employeeDataModel.Id = id;
                employeeDataModel.Code = code;
                employeeDataModel.Name = name;
                employeeDataModel.Email = email;
                employeeDataModel.Birthday = birthday;

                switch(State)
                {
                    case EntityState.Added:
                        //Commerce Rules
                        employeeRepository.Add(employeeDataModel);
                        message = "Successfully record";
                        break;
                    case EntityState.Modified:
                        employeeRepository.Edit(employeeDataModel);
                        message = "Successfully edited";
                        break;
                    case EntityState.Deleted:
                        employeeRepository.Remove(id);
                        message = "Successfully removed";
                        break;
                }
            } catch (Exception ex)
            {
                SqlException sqlEx = ex as SqlException;
                if(sqlEx != null && sqlEx.Number == 2627)
                {
                    message = "Duplicate record";
                } else
                {
                    message = ex.ToString();
                }
            }

            return message;
        }

        public List<EmployeeModel> GetAll()
        {
            var employeeDataModel = employeeRepository.GetAll();
            var listEmployees = new List<EmployeeModel>();
            foreach(Employee employee in employeeDataModel)
            {
                listEmployees.Add(new EmployeeModel
                {
                    id = employee.Id,
                    code = employee.Code,
                    name = employee.Name,
                    email = employee.Email,
                    birthday = employee.Birthday,
                    age = CalculateAge(employee.Birthday)
                });
            }

            return listEmployees;
        }

        private int CalculateAge(DateTime date)
        {
            DateTime now = DateTime.Now;
            return now.Year - date.Year;
        }
    }
}
