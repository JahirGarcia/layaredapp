﻿using DataAccess.Contracts;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class EmployeeRepository : MasterRepository, IEmployeeRepository
    {
        private string selectAll;
        private string insert;
        private string update;
        private string delete;

        public EmployeeRepository()
        {
            this.selectAll = "select * from Employee";
            this.insert = "insert into Employee (code, name, email, birthday) values(@code, @name, @email, @birthday);";
            this.update = "update from Employee set code = @code, name = @name, email = @email, birthday = @birthday where id = @id";
            this.delete = "delete from Employee where id = @id";
        }

        public int Add(Employee entity)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@code", entity.Code));
            parameters.Add(new SqlParameter("@name", entity.Name));
            parameters.Add(new SqlParameter("@email", entity.Email));
            parameters.Add(new SqlParameter("@birthday", entity.Birthday));
            return ExecuteNonQuery(insert);
        }

        public int Edit(Employee entity)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", entity.Id));
            parameters.Add(new SqlParameter("@code", entity.Code));
            parameters.Add(new SqlParameter("@name", entity.Name));
            parameters.Add(new SqlParameter("@email", entity.Email));
            parameters.Add(new SqlParameter("@birthday", entity.Birthday));
            return ExecuteNonQuery(update);
        }

        public IEnumerable<Employee> GetAll()
        {
            var table = ExecuteReader(selectAll);
            var employees = new List<Employee>();
            foreach(DataRow row in table.Rows)
            {
                employees.Add(new Employee {
                    Id = int.Parse(row[0].ToString()),
                    Code = row[3].ToString(),
                    Name = row[1].ToString(),
                    Email = row[2].ToString(),
                    Birthday = DateTime.Parse(row[4].ToString())
                });
            }
            return employees;
        }

        public int Remove(int id)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@id", id));
            return ExecuteNonQuery(delete);
        }
    }
}
